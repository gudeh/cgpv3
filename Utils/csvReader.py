import os
import pandas as pd
import matplotlib.pyplot as plt

### Set your path to the folder containing the .csv files
PATH = './Learning/' # Use your path

### Fetch all files in path
fileNames = os.listdir(PATH)

### Filter file name list for files ending with .csv
fileNames = [file for file in fileNames if '.csv' in file]

### Loop over all files
for file in fileNames:

    ### Read .csv file and append to list
#    df = pd.read_csv(PATH + file, index_col = 0)
    col_names=['Generation','FuncAnds']
    df = pd.read_csv(PATH + file,usecols=col_names, index_col = 0)#,header=None)#

    ### Create line for every file
    plt.plot(df)

### Generate the plot
plt.show()
plt.savefig(title,dpi=150)
