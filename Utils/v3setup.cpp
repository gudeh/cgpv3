/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   v3setup.cpp
 * Author: gudeh
 *
 * Created on June 15, 2021, 12:24 PM
 */

#include <cstdlib>
#include <filesystem> //requires c++17
namespace fs = std::filesystem;
#include <string>
#include <iostream>
#include <vector>
#include <random>
#include <fstream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    string path = "../inputAigs/",command,line;
    ofstream script;
    random_device device;    
    mt19937 mt(device());
    int scrip_num,index=0;
    cout<<path<<endl;
    vector<string> exemplars;
    for (const auto & entry : fs::directory_iterator(path))
    {
        string my_str=entry.path();        
        my_str=entry.path();
        cout<<"FULL NAME:"<<my_str<<endl;
        string full_name=my_str;
        exemplars.push_back("../"+full_name);
    }
    cout<<"Exemplars size:"<<exemplars.size()<<endl;
    uniform_int_distribution<int> dist_func(0,exemplars.size()-1);
    while(exemplars.size()<60)
        exemplars.push_back(exemplars[dist_func(mt)]);
    cout<<"Exemplars size:"<<exemplars.size()<<endl;
    for(int u=0;u<exemplars.size();u++)
        cout<<exemplars[u]<<endl;
    for(scrip_num=1;scrip_num<=5;scrip_num++) //number of scripts
    {
        script.open("script"+to_string(scrip_num)+".sh");
        //(X*scrip_num) where X is the number of lines, fix '-(X-1)' initial value if changed
        for(int d=1*(4*scrip_num)-3;d<(4*scrip_num)+1;d++) //each line in script
        {
            cout<<"d:"<<d<<endl;

            script<<"cd /users/gudeh/SEMCheckAll/cgp"
                    +to_string(d)<<endl<<"nohup ./cgp "
                    +exemplars[index]
                    +" "+exemplars[index+1]
                    +" " +exemplars[index+2]
//                    +" "+to_string(exemplars[index+3])
                    +" >/dev/null &"<<endl;
            index+=3;
        }
        script.close();
    }
    cout<<"CHECK INDEX:"<<index<<"=="<<exemplars.size()<<endl;
    return 0;
}

