//This piece of code is used to generate random number for the exemplars to be executed.
//It generates 5 scripts to be run by ssh with other scripts.
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: user
 *
 * Created on June 7, 2020, 10:45 PM
 */

#include <cstdlib>
#include <vector>
#include <fstream>
#include <numeric> 
#include <random>
#include <algorithm> 
#include <iostream>
#include <string>
#define WITH 0

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    vector<int> exemplars;
    string command,line;
    ofstream script;
    random_device device;    
    mt19937 mt(device());
    
    int scrip_num,index=0,single_exemplar=0;
    

//        exemplars.resize(80);
//        int x=1; 
//        iota(exemplars.begin(),exemplars.end(),x++);
        //trying to sum 60 exemplars (some are duplicated)
//        exemplars={13,20,17,60,18,67,63,79,11,71,15,68,74,78,14,76,77,54,16,41,12,53,21,52,25,44,49,9,27,                13,20,17,60,18,67,63,79,11,71,15,68,74,78,14,76,77,54,16,41,12,53,21,52,25,44,49,9,27,27,79};
//        exemplars={100,99,98,97,96,95,94,93,92,91,90,89,88,87,86,85,84,83,82,81,
//                   100,99,98,97,96,95,94,93,92,91,90,89,88,87,86,85,84,83,82,81,
//                  100,99,98,97,96,95,94,93,92,91,90,89,88,87,86,85,84,83,82,81};                  
        exemplars={91,86,85,83,82,81,91,86,85,83,82,81,91,86,85,83,82,81,91,86,85,83,82,81,91,86,85,83,82,81,91,86,85,83,82,81,91,86,85,83,82,81,91,86,85,83,82,81,91,86,85,83,82,81,91,86,85,83,82,81};
        uniform_int_distribution<int> dist_func(0,exemplars.size()-1);
//                cout<<exemplars.size()<<"->";
//        for(int d=0;d<exemplars.size();d++)
//            cout<<exemplars[d]<<","; cout<<endl<<endl;       
//        exemplars.push_back(exemplars[dist_func(mt)]);        exemplars.push_back(exemplars[dist_func(mt)]);
        cout<<exemplars.size()<<"->";
        for(int d=0;d<exemplars.size();d++)
            cout<<exemplars[d]<<","; cout<<endl<<endl;

        shuffle(exemplars.begin(),exemplars.end(),mt);

        cout<<exemplars.size()<<"->";
        for(int d=0;d<exemplars.size();d++)
            cout<<exemplars[d]<<","; cout<<endl<<endl;
    for(scrip_num=1;scrip_num<=5;scrip_num++) //number of scripts
    {
        script.open("script"+to_string(scrip_num)+".sh");
        //(X*scrip_num) where X is the number of lines, fix '-(X-1)' initial value if changed
        for(int d=1*(4*scrip_num)-3;d<(4*scrip_num)+1;d++) //each line in script
        {
            cout<<"d:"<<d<<endl;
#if WITH == 1
            script<<"cd /users/gudeh/COMCheckAll/cgp"+to_string(d)<<endl<<"nohup ./cgp "
                    +to_string(exemplars[index])+" "
                    +to_string(exemplars[index+1])+" "+to_string(exemplars[index+2])+" "+to_string(exemplars[index+3])+" >/dev/null &"<<endl;
#else
            script<<"cd /users/gudeh/SEMCheckAll/cgp"
                    +to_string(d)<<endl<<"nohup ./cgp "
                    +to_string(exemplars[index])
                    +" "+to_string(exemplars[index+1]) 
                    +" " +to_string(exemplars[index+2])
//                    +" "+to_string(exemplars[index+3])
                    +" >/dev/null &"<<endl;
//               script<<"cd /users/gudeh/SEMCheckAll/cgp"+to_string(d)<<endl<<"nohup ./cgp "+to_string(exemplars[index])+" "+to_string(exemplars[index+1])+" >/dev/null &"<<endl;
#endif
//            command="./script.sh ";//+to_string(exemplars[index])+" "+to_string(exemplars[index+1])+" "+to_string(exemplars[index+2])+" "+to_string(exemplars[index+3])+" &";
//            system(command.c_str());
            index+=3;
        }
        script.close();
    }
    cout<<"CHECK INDEX:"<<index<<"=="<<exemplars.size()<<endl;
//        for(int a=0;a<20;a++)
//            exemplars.erase(exemplars.begin());
//        ofstream outFile("tracker.txt");
//        for(int d=0;d<exemplars.size();d++)
//            outFile<<exemplars[d]<<endl;
    return 0;
}

