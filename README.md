A Cartesian Genetic Programming (CGP) implementation. Input file in PLA format with an incomplete binary behaviour (truth tables, mainly from IWLS 2020 dataset), output is a learned AND-Inverter Graph (AIG). The code can start with a completly random population (only PLA file as input), or with a previously generated AIG together with the PLA. 

This project was published in: 
- Invited for publication at: Journal of Integrated Circuits and Systems (JICS). Currently under first round peer review.
- https://doi.org/10.23919/DATE51398.2021.9473972
- https://doi.org/10.1109/SBCCI53441.2021.9529968
- Regional congress: "Bootstrapping Logic Optimization Techniques With Cartesian Genetic Programming" at EMicro/SIM 2021.

This version 3 comes from a previous repository, in this new version the code is more compact by removing one unecessary class. 
